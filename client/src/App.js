import logo from "./logo.svg";
import React, { useEffect, useState } from "react";
import * as axios from "axios";
import "./App.css";

function App() {
  const [data, setData] = useState([]);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [id, setId] = useState(null)
  const [usernameSearch, setUsernameSearch] = useState("");
  const [emailSearch, setEmailSearch] = useState("");
  const [dataSearch, setDataSearch] = useState([]);

  useEffect(async () => {
    const result = await axios("http://localhost:5000/api/players");
    setData(result.data.message);
  }, [data]);

  const save = async () => {
    const saveData = await axios.post('http://localhost:5000/api/players', {
      username,
      email,
      password
    })

    if(saveData.status === 201){
      const currentData = data;
      currentData.push(saveData.data.message);
      setData(currentData)
    }
  }

  const edit = (key) => {
    const dataEdit = data[key];
    setUsername(dataEdit.username);
    setEmail(dataEdit.email);
    setPassword(dataEdit.password);
    setId(dataEdit.id);
  }

  const update = async () => {
    const updateData = await axios.put(`http://localhost:5000/api/players/${id}`, {
      username,
      email,
      password
    });
  }

  const search = () => {
    const currentDataSearch = data.filter(item => item.username.includes(usernameSearch) 
      || item.email.includes(emailSearch));
    setDataSearch(currentDataSearch);
  }

  return (
    <div className="App">
      <div className="container">
        player
        <ul>
          {data?.map((item, key) => (
            <li>
              {item.username} | {item.email} | <button id={key} onClick={(e) => edit(e.target.id)}>edit</button>
            </li>
          ))}
        </ul>
        <br /> <br /> <br />
        <h2>Tambah player</h2>
        username <input type="text" value={username} onChange={(e) => setUsername(e.target.value)}/><br/>
        email <input type="email" value={email} onChange={(e) => setEmail(e.target.value)}/><br/>
        password <input type="password" value={password} onChange={(e) => setPassword(e.target.value)}/><br/>
        <button onClick={save}>Simpan</button>
        <button onClick={update}>Edit</button>
      

      <h2>Pencarian</h2>
        username <input type="text" value={usernameSearch} onChange={(e) => setUsernameSearch(e.target.value)}/><br/>
        email <input type="text" value={emailSearch} onChange={(e) => setEmailSearch(e.target.value)}/><br/>
        <button onClick={search}>submit</button>

        <h3>Hasil pencarian</h3>
        <ul>
        {dataSearch?.map(item => (
          <li>{item.username}</li>
        ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
